﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
        print("It's starting!");
	}

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnMouseDown();
        } 
        else if (Input.GetMouseButtonDown(1))
        {
            OnMouseUp();
        }
    }
    private void OnMouseDown()
    {
        transform.Rotate(new Vector3Int(90, 0, 0));
    }

    private void OnMouseUp()
    {
        transform.Rotate(new Vector3Int(-90, 0, 0));
    }
}
